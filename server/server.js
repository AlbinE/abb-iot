const path = require('path');
const express = require('express');
const handlebars = require('express-handlebars');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');
const apiGateway = require('aws-api-gateway-client').default;

// Configuration
const config = {
    // Server port
    port: 3000,
    // AWS API Gateway stage
    apiStage: 'abbiot',
    // AWS API Gateway Key
    apiKey: 'gmbAXMWGVh8inCCXPIBnJ6VuYECb721n2aPLCf37',
    // Logging to console
    enableDebugLog: false,
    enableInfoLog: false,
    enableErrorLog: true,
};

// Create web app
if(config.enableDebugLog) {
    console.debug("Initializing express");
}
const app = express();

// Create API Gateway client
if(config.enableDebugLog) {
    console.debug("Creating AWS API Gateway client");
}
const apiClient = apiGateway.newClient({
    invokeUrl: 'https://08n8w6g1nb.execute-api.eu-north-1.amazonaws.com',
    apiKey: config.apiKey,
    region: 'eu-north-1'
})

// Configure handlebars
if(config.enableDebugLog) {
    console.debug("Configuring handlebars");
}
app.engine('.hbs', handlebars({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

// Configure body parser
if(config.enableDebugLog) {
    console.debug("Configuring body and cookie parsers");
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Configure cookie parser and sessions
app.use(cookieParser());
// Sessions store temporary information about the user such as their session ID.
// The session ID is used to know whether or not the user is logged in.
app.use(session({secret: "abbiot-$z1O69fdw9&p6qM8#r8rBp7t"}));

// TODO: Add expiry time for session tokens.
// Leaked session tokens will enable anyone to log in by editing their cookies.

// Error callback
app.use((err, req, res, next) => {
    if(config.enableErrorLog) {
        console.error(err);
    }
    res.status(500).send('Something in the server broke! Check the error logs.');
});

// Set static files (CSS stylesheets, images, JS scripts)
app.use(express.static(__dirname + '/public'));

// Contains all logged in sessions
activeSessions = [];
// Gets options to be passed to handlebars rendering engine
function getSessionOptions(session, sessionID, directory, more = {}) {
    var out = {
        // Specifies whether or not the user is logged in
        loggedIn: activeSessions.includes(sessionID),
        // Specifies with which e-mail the user logged in with
        loginEmail: session.email,
        // TODO: Check if this is necessary
        debug: true,

        // This helper will highlight which option from the navbar is active
        helpers: {
            navactive: function(dir) { return directory === dir ? "active" : ""; }
        }
    };
    // Add other keys specified in the 'more' object
    for(var key in more) out[key] = more[key];
    // Return the session options
    return out;
}

// Homepage (Login page)
app.get('/', (req, res) => {
    // If already logged in
    if(activeSessions.includes(req.sessionID)) {
        // Redirect to overview
        res.redirect('/overview');
    }
    // If not logged in
    else {
        // Show login page
        res.render('login', getSessionOptions(req.session, req.sessionID, 'login'));
    }
});

// Login function
app.post('/login', (req, res) => {
    // If user is not logged in
    if(!activeSessions.includes(req.sessionID)) {
        // Add email to session object
        req.session.email = req.param("inputEmail");

        // Log to console
        if(config.enableInfoLog) {
            console.log(`${req.session.email} logged in`);
        }

        // Add user to active sessions
        activeSessions.push(req.sessionID);
    }
    // Redirect user to overview
    res.redirect('/overview');
})

// Logout function
app.get('/logout', (req, res) => {
    // If user is logged in
    if(activeSessions.includes(req.sessionID)) {
        // Remove user from active sessions
        activeSessions.splice(activeSessions.indexOf(req.sessionID), 1);

        // Log to console
        if(config.enableInfoLog) {
            console.log(`${req.session.email} logged out`);
        }

        // Remove email from session object
        req.session.email = null;
    }
    // Redirect user to login page
    res.redirect('/');
})

// Device overview (protected page)
app.get('/overview', async (req, res) => {
    // If user session is logged in
    if(req.session.id && activeSessions.includes(req.session.id)) {
        // Lists of registered and unregistered devices
        var registered;
        var unregistered;

        // Retrieve registered device list
        await apiClient.invokeApi({}, `/${config.apiStage}/device/registered`, 'GET', {}, {})
        .then((result) => {
            // Connection succeeded
            registered = result.data.slice();
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(console.enableErrorLog) {
                console.error(result);
            }
        });

        // Retrieve unregistered device list
        await apiClient.invokeApi({}, `/${config.apiStage}/device/unregistered`, 'GET', {}, {})
        .then((result) => {
            // Connection succeeded
            unregistered = result.data.slice();
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(console.enableErrorLog) {
                console.error(result);
            }
        });

        // Show overview page and supply the frontend with device lists
        res.render('overview', getSessionOptions(req.session, req.sessionID, 'overview', {
            devices: registered,
            unregistered: unregistered,
            scripts: ['/js/visualization.js']
        }));
    }
    // If not logged in
    else {
        // Redirect to login page
        res.redirect('/');
    }
});

// Pseudo-API function to get map points (protected page)
//   This middleware function protects end users from getting the API key
//   but still get access to protected API functions by logging in
// Example use: GET /overview/maps/floor1
// Returns devices with position and latestData in a JSON list
app.get('/overview/maps/:mapId', async (req, res) => {
    // If user is logged in
    if(req.session.id && activeSessions.includes(req.session.id)) {
        // List of map points
        var points;

        // Retrieve map point list
        await apiClient.invokeApi({}, `/${config.apiStage}/map/points`, 'GET', {
            queryParams: {
                MapID: req.params.mapId
            }
        }, {})
        .then((result) => {
            // Connection succeeded
            points = result.data.slice();
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(config.enableErrorLog) {
                console.error(result);
            }
        });

        // Respond in JSON format
        res.send(JSON.stringify(points));
    // If not logged in
    } else {
        // Give unauthorized error
        res.status(401)
        res.send('Unauthorized')
    }
});

// Pseudo-API function to get device (protected page)
//   This middleware function protects end users from getting the API key
//   but still get access to protected API functions by logging in
// Example use: GET /overview/device/12:34:56:78:9A:BC
// Returns device in a JSON object
app.get('/overview/device/:macAddress', async (req, res) => {
    // If user is logged in
    if(req.session.id && activeSessions.includes(req.session.id)) {
        // Device object
        var device = {};

        // Retrieve the device
        await apiClient.invokeApi({}, `/${config.apiStage}/device`, 'GET' , {
            queryParams: {
                MAC: req.params.macAddress
            }
        }, {})
        .then((result) => {
            // Connection succeeded, copying data
            Object.assign(device, result.data);
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(config.enableErrorLog) {
                console.error(result);
            }
        });

        // Respond in JSON format
        res.send(JSON.stringify(device));
    // If not logged in
    } else {
        // Give unauthorized error
        res.status(401)
        res.send('Unauthorized')
    }
})

// Pseudo-API function to register device (protected page)
//   This middleware function protects end users from getting the API key
//   but still get access to protected API functions by logging in
// Example use: POST /overview/device/12:34:56:78:9A:BC/FirstName LastName/Device Name
app.post('/overview/device/:macAddress/:owner/:name', async (req, res) => {
    // If user is logged in
    if(req.session.id && activeSessions.includes(req.session.id)) {
        // Register device
        await apiClient.invokeApi({}, `/${config.apiStage}/device/registered`, 'PUT', {
            queryParams: {
                MAC: req.params.macAddress
            }
        }, {
            owner: req.params.owner,
            name: req.params.name
        })
        .then((result) => {
            // Connection succeeded
            res.status(201);
            res.send('Device Registered');
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(config.enableErrorLog) {
                console.error(result);
            }
        })
    // If not logged in
    } else {
        // Give unauthorized error
        res.status(401)
        res.send('Unauthorized')
    }
})

// Pseudo-API function to edit device settings (protected page)
//   This middleware function protects end users from getting the API key
//   but still get access to protected API functions by logging in
// :interval is in seconds
// :x and :y are decimal numbers between 0.0 and 1.0
// Example use: PUT /overview/device/options/12:34:56:78:9A:BC/1800/Device Name/0.5/0.5/floor1/FirstName LastName
app.put('/overview/device/options/:macAddress/:interval/:name/:x/:y/:map/:owner', async (req, res) => {
    // If user is logged in
    if(req.session.id && activeSessions.includes(req.session.id)) {
        // Edit device settings
        await apiClient.invokeApi({}, `/${config.apiStage}/device/options`, 'PUT', {
            queryParams: {
                MAC: req.params.macAddress
            }
        }, {
            // Settings
            interval: parseInt(req.params.interval),
            name: req.params.name,
            position: {
                x: parseFloat(req.params.x),
                y: parseFloat(req.params.y)
            },
            map: req.params.map,
            owner: req.params.owner
        })
        .then((result) => {
            // Connection succeeded
            res.status(200);
            res.send('Device Updated')
        })
        .catch((result) => {
            // Connection failed
            res.status(500);
            res.send('Internal Server Error')

            // Log to console
            if(config.enableErrorLog) {
                console.error(result);
            }
        })
    // If not logged in
    } else {
        // Give unauthorized error
        res.status(401)
        res.send('Unauthorized')
    }
})

// Start server
app.listen(process.env.PORT || config.port, (err) => {
    // Log failures to console
    if(err && config.enableErrorLog) {
        return console.error('Something bad happened!', err);
    }

    // Log to console when it's started
    console.log(`Server is listening on port ${config.port}.`);
});
