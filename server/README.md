# Web Server on Elastic Beanstalk
To build and run this package you will need to install [Node.js](https://nodejs.org/en/) on your computer.

## Installation
```
cd server
npm install
```

## Test locally
```
cd server
node server.js
```
