"use strict";

// Change these
let colors = [
    {color: "rgb(227,35,27)", value: null}, // highest value
    {color: "rgb(228,58,28)", value: null},
    {color: "rgb(231,96,31)", value: null},
    {color: "rgb(236,137,35)", value: null},
    {color: "rgb(241,178,39)", value: null},
    {color: "rgb(248,222,44)", value: null},
    {color: "rgb(245,253,47)", value: null},
    {color: "rgb(210,252,44)", value: null},
    {color: "rgb(176,251,42)", value: null},
    {color: "rgb(146,250,40)", value: null},
    {color: "rgb(120,250,38)", value: null},
    {color: "rgb(103,249,37)", value: null},
    {color: "rgb(100,249,44)", value: null},
    {color: "rgb(100,249,77)", value: null},
    {color: "rgb(101,250,157)", value: null},
    {color: "rgb(101,250,200)", value: null},
    {color: "rgb(101,251,242)", value: null},
    {color: "rgb(89,219,251)", value: null},
    {color: "rgb(71,175,250)", value: null},
    {color: "rgb(55,133,249)", value: null},
    {color: "rgb(38,91,249)", value: null},
    {color: "rgb(23,50,248)", value: null},
    {color: "rgb(14,22,248)", value: null} // lowest value
]

let maps = [
    {
        id: "floor1",
        filename: '/images/Map2.png',
        title: 'Våning 1',
    },
    {
        id: "floor2",
        filename: '/images/Map1.png',
        title: 'Våning 2',
    },
    {
        id: "floor3",
        filename: '/images/Map3.png',
        title: 'Labb',
    }
]

let updateIntervalFormats = [  // measured in seconds
    {
        title: "Sekund",
        value: 1,
    },
    {
        title: "Minut",
        value: 60,
    },
    {
        title: "Timme",
        value: 3600,
    },
    {
        title: "Dag",
        value: 86400,
    }
]

let sensors = [
    {
        title: "Temperatur",
        type: "temperature",
        unit: "°C",
        min: 0,
        max: 35,
        k: 1,
        m: 0
    },
    {
        title: "Koldioxid",
        type: "co2",
        unit: "ppm",
        min: 0,
        max: 1000,
        k: 1,
        m: 0
    },
    {
        title: "Luftfuktighet",
        type: "humidity",
        unit: "%",
        min: 0,
        max: 100,
        k: 1,
        m: 0
    }
]

let mapIndex = 0
let updateIntervalIndex = 0
let sensorIndex = 0
let startDate = new Date() - 24 * 3600000
let endDate = new Date()
let numberOfYTicks = 10
let numberOfDataPoints = 10
let minUpdateInterval = 10
// Stop changing

let minValue
let maxValue
let xStepSize = Math.ceil((endDate - startDate) / numberOfDataPoints)
let yStepSize = Math.ceil((maxValue - minValue) / numberOfYTicks)

let devices = []
let displayData = []
let newDates = []

let differenceBetweenColors
let idCount = 0
let mapIsHidden = true

createMapDropdown('mapsDropdown')
createMapDropdown('modalMapsDropdown')
createUpdateIntervalDropdown('updateIntervalDropdown')
createSensorDropdown('sensorDropdown')
createSensorDropdown('deviceModalSensorDropdown')

let ctx
let scatterChart
let currentDevice
let currentCoordinates

let dataset = {
    label: "Placeholder",
    devices: displayData,
    borderColor: 'rgba(243, 149, 45, 1)',
    borderWidth: 1,
    backgroundColor: 'rgba(243, 149, 45, 0.4)',
    pointBackgroundColor: 'rgba(0, 0, 0, 1)',
    pointBorderColor: 'rgba(0, 0, 0, 1)',
    pointRadius: 3,
    pointHoverRadius: 5,
    fill: true,
    tension: 0.5,
    showLine: true,
    scaleOverride: true,
    scaleSteps:10,
    scaleStartValue:0,
    scaleStepWidth:1,
}

initializeScatterChart()

let currentMap
let updateIntervalFormat
let currentSensor

setCurrentMap(mapIndex)
setUpdateIntervalFormat(updateIntervalIndex)
setCurrentSensor(sensorIndex)

getMapDevices(currentMap.id)

// functions

/*
Gets a verson of each device on the current map but with only the latest data
mapId is the id variable of a map object
*/
function getMapDevices(mapId){
    $.getJSON(
        "/overview/maps/" + mapId,
        function(data){
            devices = data
            devices.forEach((device, i) => {
                device.id = i
                device.mapId = mapId
            })
            drawAllDevices()
        }
    )
}

/*
Gets all the data from a certain device
macAddress is the macAddress variable of a device object
*/
function getDevice(macAddress) {
    $.getJSON(
        "/overview/device/" + macAddress,
        (data) => {
            currentDevice = data
            currentCoordinates = {  // the coordinates of the current device. This is used on the modal map
                x: currentDevice.position.x,
                y: currentDevice.position.y,
                mapId: currentDevice.mapId
            }

            currentDevice.data.forEach((data) => {
                data.time *= 1000  // convert the data to milliseconds
            })

            // updates the displayed information in the modal
            $("#deviceModalCenterTitle").text(`Redigera "${currentDevice.name}"`)
            $("#deviceModalMac").val(currentDevice.macAddress)
            $("#deviceModalOwnerInput").val(currentDevice.owner)
            $("#deviceModalNameInput").val(currentDevice.name)
            $("#deviceModalIntervalInput").val(currentDevice.updateInterval / updateIntervalFormat.value)
            $("#yMin").val(minValue)
            $("#yMax").val(maxValue)
            setYScatter()
            setXScatter()
        }
    )
}

/*
Will update the current device with the data from the device modal and save it to the database
*/
function saveCurrentDevice(){
    currentDevice.position = {
        x: currentCoordinates.x,
        y: currentCoordinates.y,
    }

    if ($('#deviceModalIntervalInput').val()){
        currentDevice.updateInterval = $('#deviceModalIntervalInput').val() * updateIntervalFormat.value
    }

    if (currentDevice.updateInterval < minUpdateInterval){
        currentDevice.updateInterval = minUpdateInterval
    }
    $('#deviceModalIntervalInput').val(currentDevice.updateInterval / updateIntervalFormat.value)

    if ($('#deviceModalOwnerInput').val()){
        currentDevice.owner = $('#deviceModalOwnerInput').val()
    }

    if ($('#deviceModalNameInput').val()){
        currentDevice.name = $('#deviceModalNameInput').val()
    }

    currentDevice.mapId = currentCoordinates.mapId

    $.ajax({
        url: `/overview/device/options/${currentDevice.macAddress}/${currentDevice.updateInterval}/${currentDevice.name}/${currentDevice.position.x}/${currentDevice.position.y}/${currentDevice.mapId}/${currentDevice.owner}`,
        type: "PUT",
        success: function(data){}
    })
    let currentDeviceIndex = devices.indexOf(devices.find(obj => obj.macAddress == currentDevice.macAddress))  // finds the index of the currentDevice in devices. Since the getDevice and getMapDevices return different formats this code is necessary

    if (currentDeviceIndex == -1){ // the index will be -1 if the current device is being moved to the current map
        devices.push(currentDevice)
    }
    else{
        devices[currentDeviceIndex] = currentDevice
    }
    drawAllDevices()
    drawDevice('modalMapImg', currentCoordinates, currentCoordinates.mapId, currentDevice)
}

/*
Will trigger when the update button on the map tab is pressed and will update the minValue and maxValue
*/
function setMaxMin() {
    if($("#inputMin").val()) {
        minValue = parseInt($("#inputMin").val())
    }

    if($("#inputMax").val()) {
        maxValue = parseInt($("#inputMax").val())
        if(maxValue <= minValue) {
            maxValue = minValue + 1
            $("#inputMax").val(maxValue)
        }
    }

    drawAllDevices()
}

/*
Will use the data from the device modal to change the maxValue and minValue
*/
function setYScatter() {
    if($("#yMin").val()) {
        minValue = parseInt($("#yMin").val())
    }

    if($("#yMax").val()) {
        maxValue = parseInt($("#yMax").val())
        if(maxValue <= minValue) {
            maxValue = minValue + 1
        }
    }

    $("#yMax").val(maxValue)

    $("#inputMin").val(minValue)
    $("#inputMax").val(maxValue)

    yStepSize = Math.ceil((maxValue - minValue) / numberOfYTicks)

    scatterChart.options.scales.yAxes[0].ticks = {
        min: minValue,
        max: maxValue,
        stepSize: yStepSize,
    }

    scatterChart.update()

    drawAllDevices()
    if ($('#deviceModal').hasClass('show')){  // if the modal is shown
        drawDevice("modalMapImg", currentCoordinates, currentCoordinates.mapId, currentDevice)
    }
}

/*
Uses the data from the device modal to change the x-axes and the displayed data
*/
function setXScatter() {
    if(currentDevice) {
        newDates = []
        if($('#numberOfDataPoints').val()) {
            numberOfDataPoints = $('#numberOfDataPoints').val()
        }

        let timeBetweenPoints = (endDate - startDate) / (numberOfDataPoints - 1)  // the -1 is there to skip the gap at the beginning or end

        // find new datapoints
        let nextDate
        for(let i = 0; i < numberOfDataPoints; i++) {
            nextDate = new Date(endDate.getTime() - timeBetweenPoints * i)
            newDates.push(findClosestDate(nextDate))
        }

        while(newDates.includes(undefined)){  // findClosestDate will return undefined values if the date already is in newDates
            newDates.splice(newDates.indexOf(undefined), 1)
        }

        if(newDates.length > 0){
            displayData = [{x: startDate, y: undefined}, {x: endDate, y: undefined}]  // For some reason Chart.js won't update the xAxis interval to the max and min value, it will however update the interval to the max and min datapoints which is why this works.
            newDates.forEach((newDate, i) => {
                let date = new Date(newDate.time)
                displayData.push({
                    x: date.getTime(),
                    y: newDates[i][currentSensor.type],
                })
            })
        }
        else {
            displayData = [{x: startDate, y: undefined}, {x: endDate, y: undefined}]
        }
    }
    else {
        displayData = [{x: startDate, y: undefined}, {x: endDate, y: undefined}]
    }

    dataset.data = displayData
    scatterChart.data.datasets = [dataset]
    scatterChart.update()
}

/*
Creates the scatterChart object
*/
function initializeScatterChart() {
    ctx = document.getElementById("chartScatter").getContext('2d')
    scatterChart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: [dataset]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'linear',
                }]
            },
            responsive: true,
        }
    })

    setYScatter()
    setXScatter()
}

/*
Used to create the dropdowns containing the different maps from the array maps
idHTML is the id of the dropdown menu HTML element
*/
function createMapDropdown(idHTML) {
    let mapsDropdown = document.getElementById(idHTML)
    maps.forEach((map, i) => {
        let newMap = document.createElement("a")

        newMap.className = "dropdown-item"
        newMap.innerHTML = map.title
        newMap.href = "#"
        newMap.id = i
        newMap.onclick = (event) => {
            event.preventDefault()
            setCurrentMap(newMap.id)
        }

        mapsDropdown.appendChild(newMap)
    })
}

/*
Sets the current map
index is the index of the map you want to set to current in the array maps
*/
function setCurrentMap(index) {
    currentMap = maps[index]

    document.getElementById('mapImg').src = currentMap.filename
    document.getElementById('modalMapImg').src = currentMap.filename

    getMapDevices(currentMap.id)
    drawAllDevices()

    if ($('#deviceModal').hasClass('show')){  // if the modal is shown
        drawDevice("modalMapImg", currentCoordinates, currentCoordinates.mapId, currentDevice)
    }
}

/*
Used to create the dropdowns containing the different update interval formats from the array updateIntervalFormats
idHTML is the id of the dropdown menu HTML element
*/
function createUpdateIntervalDropdown(idHTML){
    let dateDropdown = document.getElementById(idHTML)
    updateIntervalFormats.forEach((format, i) => {
        let newDate = document.createElement("a")
        newDate.className = "dropdown-item"
        newDate.innerHTML = updateIntervalFormats[i].title
        newDate.id = updateIntervalFormats[i].title
        newDate.href = "#"
        newDate.value = length
        newDate.onclick = (event) => {
            setUpdateIntervalFormat(i)
            event.preventDefault()
        }
        dateDropdown.appendChild(newDate)
    })
}

/*
Sets the current update interval format
index is the index of the update interval format you want to set to current in the array updateIntervalFormats
*/
function setUpdateIntervalFormat(index){
    if (!updateIntervalFormat){
        updateIntervalFormat = updateIntervalFormats[0] // needs to be defined for the rest of the function to work
    }
    let oldFormat = updateIntervalFormat
    updateIntervalFormat = updateIntervalFormats[index]
    document.getElementById("updateIntervalDropdownButton").innerHTML = updateIntervalFormats[index].title
    $('#deviceModalIntervalInput').val($('#deviceModalIntervalInput').val() * oldFormat.value / updateIntervalFormat.value) // convert the displayed value when changing updateIntervalFormat (for example if the format is minute and value is 1, changing the format to second will change the value to 60)
}

/*
Used to create the dropdowns containing the different sensor types from the array sensors
idHTML is the id of the dropdown menu HTML element
*/
function createSensorDropdown(idHTML){
    let sensorDropdown = document.getElementById(idHTML)

    sensors.forEach((sensor, i) => {
        let dropdownItem = document.createElement("a")
        dropdownItem.className = "dropdown-item"
        dropdownItem.innerHTML = `${sensor.title} (${sensor.unit})`
        dropdownItem.id = sensor.title
        dropdownItem.href = "#"
        dropdownItem.value = length
        dropdownItem.onclick = (event) => {
            event.preventDefault()
            setCurrentSensor(i)
        }

        sensorDropdown.appendChild(dropdownItem)
    })
}

/*
Sets the sensor type
index is the index of the sensor type you want to set to current in the array sensors
*/
function setCurrentSensor(index) {
    currentSensor = sensors[index]

    document.getElementById("inputMin").value = currentSensor.min
    document.getElementById("inputMax").value = currentSensor.max
    document.getElementById("yMin").value = currentSensor.min
    document.getElementById("yMax").value = currentSensor.max

    minValue = currentSensor.min
    maxValue = currentSensor.max
    setMaxMin()

    setYScatter()
    setXScatter()

    dataset.label = currentSensor.title
    scatterChart.update()
}

/*
Find the datapoint in the currentDevice.data which time is the closest to the desired time. Will only find data within startDate and endDate and data with a defined value
desiredDate is a date object
*/
function findClosestDate(desiredDate){
    let closestDate

    for (let i = 0; i < currentDevice.data.length; i++){
        if (currentDevice.data[i].time > startDate && currentDevice.data[i].time < endDate) {
            if (closestDate) {
                if (Math.abs(currentDevice.data[i].time - desiredDate) < Math.abs(closestDate.time - desiredDate)) {
                    closestDate = currentDevice.data[i]
                }
                else {
                    break
                }
            }
            else {
                closestDate = currentDevice.data[i]
            }
        }
    }
    if (newDates.includes(closestDate)){
        return  // will return undefined but setXScatter removes all undefined from newDates
    }
    else{
        return closestDate
    }
}

/*
Will update the current coordinates and move the dot on the modal map when you click on it
inputX is the pixels from the left of where you click
inputY is the pixels from the top of where you click
*/
function createDeviceOnClick(inputX, inputY){
    let x = (inputX - (document.getElementById("modalMapImg").getBoundingClientRect().left)) / document.getElementById("modalMapImg").getBoundingClientRect().width
    let y = (inputY - (document.getElementById("modalMapImg").getBoundingClientRect().top)) / document.getElementById("modalMapImg").getBoundingClientRect().height
    currentCoordinates = {
        x: x,
        y: y,
        mapId: currentMap.id
    }
    drawDevice('modalMapImg', currentCoordinates, currentCoordinates.mapId, currentDevice)
}

/*
Draw all devices from the array devices on the map on the map tab
*/
function drawAllDevices(){
    // removes all the old the devices from the map
    let oldDevice = document.getElementById('mapImgDevices')
    while (oldDevice.firstChild) {
        oldDevice.removeChild(oldDevice.firstChild)
    }

    // create new devices
    for (let i = 0; i < devices.length; i++){
        drawDevice('mapImg', devices[i].position, devices[i].mapId, devices[i])
    }
}

/*
Draw a specific device on a specific map
mapIdHTML is the id of the img HTML element
*/
function drawDevice(mapIdHTML, coordinates, map, device){ // coordinates is x and y between 0 and 1
    // removes the old from the modalMap
    if (mapIdHTML == 'modalMapImg'){
        let oldDevice = document.getElementById('modalMapImgDevices')
        while (oldDevice.firstChild) {
            oldDevice.removeChild(oldDevice.firstChild)
        }
    }

    if (map == currentMap.id) {
        let newDevice = document.createElement("li")
        newDevice.className = "device"

        // Set this 'size' variable to the diameter of the map points
        let size = 20
        let scrollY = -size / 2
        let scrollX = -size / 2
        if (mapIdHTML == 'modalMapImg') {
            scrollY += document.getElementById('deviceModal').scrollTop - 93
            scrollX += - (window.innerWidth - document.getElementById('modalMapImg').getBoundingClientRect().width) / 2 + 34
            if ($('#devices-tab').hasClass('active')){
                scrollX -= 10
            }
        }
        else {
            scrollY += window.scrollY 
            scrollX += window.scrollX
        }

        newDevice.style.left = coordinates.x * document.getElementById(mapIdHTML).getBoundingClientRect().width + document.getElementById(mapIdHTML).getBoundingClientRect().left + scrollX + "px"
        newDevice.style.top = coordinates.y * document.getElementById(mapIdHTML).getBoundingClientRect().height + document.getElementById(mapIdHTML).getBoundingClientRect().top + scrollY + "px"
        newDevice.id = device.id

        if (device.latestData){
            if (device.latestData[currentSensor.type]){
                newDevice.style.setProperty("background-color", findClosestColor(device.latestData[currentSensor.type] * currentSensor.k + currentSensor.m))
            }
        }

        // Add tooltip to newDevice
        if (!mobilecheck()) {
            newDevice.setAttribute("devices-toggle", "tooltip")
            newDevice.setAttribute("devices-placement", "top")
            if (device.latestData) {
                if (device.latestData[currentSensor.type]) {
                    newDevice.setAttribute("title", "Namn: " + device.name + " Värde: " + (device.latestData[currentSensor.type] * currentSensor.k + currentSensor.m) + " " + currentSensor.unit)
                }
            } else {
                newDevice.setAttribute("title", "Namn: " + device.name + " Värde: Ej tillgänglig")
            }
        }

        if (mapIdHTML == "mapImg") {
            let jqueryDevice = $(newDevice)
            jqueryDevice.on('click', (event) => {
                openDevice(device.macAddress)
            })
        }

        let list = document.getElementById(mapIdHTML + 'Devices')
        list.appendChild(newDevice)

        $(document).ready(function () {
            $('[devices-toggle="tooltip"]').tooltip()
        })
    }
}

/*
Uses the maxValue and minValue to update which value each color is assigned to
*/
function updateColors(){
    differenceBetweenColors = (maxValue - minValue) / (colors.length - 1)

    for (let i = 0; i < colors.length; i++) {
        colors[i].value = maxValue - differenceBetweenColors * i
    }
}

/*
Find the color with the value that is closest to a certain input value and returns that rgd color
*/
function findClosestColor(value){
    updateColors()

    if (value <= colors[colors.length - 1].value){
        return colors[colors.length - 1].color
    }
    if (value >= colors[0].value){
        return colors[0].color
    }

    for (let i = 0; i < colors.length; i++){
        if (Math.abs(colors[i].value - value) < differenceBetweenColors / 2){
            return colors[i].color
        }
    }
}

/*
Hides or unhides the map when pressing 'Lägg till på karta'
*/
function hideMap()  {
    if(mapIsHidden){
        $("#modalMapImg").show()
        $("#removeButton").prop("disabled", false)
        $("#undoButton").prop("disabled", false)
        $("#modalDropdownMenuButton").prop("disabled", false)
        mapIsHidden = false

        drawDevice('modalMapImg', currentCoordinates, currentCoordinates.mapId, currentDevice)
    } else {
        $("#modalMapImg").hide()
        $("#removeButton").prop("disabled", true)
        $("#undoButton").prop("disabled", true)
        $("#modalDropdownMenuButton").prop("disabled", true)
        mapIsHidden = true

        // Remove the drawn device
        let oldDevice = document.getElementById('modalMapImgDevices')
        while (oldDevice.firstChild) {
            oldDevice.removeChild(oldDevice.firstChild)
        }
    }
}

/*
Opens the device modal and gets the information for the device
macAddress is the macAddress variable of the device you want to open
*/
function openDevice(macAddress){
    getDevice(macAddress)
    $('#deviceModal').modal('show')
}

/*
Opens the unregistered device modal and gets the information for the device
macAddress is the macAddress variable of the device you want to open
*/
function openUnregisteredDevice(macAddress) {
    $("#registerModalMac").val(macAddress)
    $('#registerModal').modal('show')
}

// Adds onlick to register button
$('#registerButton').click((event) => {
    var macAddress = $('#registerModalMac').val()
    var owner = $('#registerModalOwnerInput').val()
    var name = $('#registerModalNameInput').val()
    $.ajax({
        url: `/overview/device/${macAddress}/${owner}/${name}`,
        type: "POST",
        success: function(data) {
            window.location.reload(false)
        }
    })
})

// Redraws the current device when the modal gets resized
$("#modalMapImg").resize(() => {
    drawDevice("modalMapImg", currentCoordinates, currentCoordinates.mapId, currentDevice)
})

// Draws all devices on the map tab when switching to that tab
$('#map-tab').on('shown.bs.tab', (event) => {
    drawAllDevices()
})

$(window).resize(function() {
    drawAllDevices()
})

// Hides the map when the modal is closed so that it will be hidden the next time the modal is opened
$('#deviceModal').on('hidden.bs.modal', function (e) {
    mapIsHidden = false
    hideMap()
})

$('#deviceModalExportCsv').click(() => {
    window.location.replace(`https://08n8w6g1nb.execute-api.eu-north-1.amazonaws.com/abbiot/device/data/csv?MAC=${currentDevice.macAddress}`);
})

$("#datetimepicker1").on("change.datetimepicker", function (e) {
    startDate = new Date(e.date.valueOf())
})

$("#datetimepicker2").on("change.datetimepicker", function (e) {
    endDate = new Date(e.date.valueOf())
})

window.mobilecheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera)
    return check
}

// for mobile collapse
$('#navbarSupportedContent').on("show.bs.collapse", function (e) {
    let oldDevice = document.getElementById('mapImgDevices')
    while (oldDevice.firstChild) {
        oldDevice.removeChild(oldDevice.firstChild)
    }
})

$('#navbarSupportedContent').on("hide.bs.collapse", function (e) {
    let oldDevice = document.getElementById('mapImgDevices')
    while (oldDevice.firstChild) {
        oldDevice.removeChild(oldDevice.firstChild)
    }
})

$('#navbarSupportedContent').on("shown.bs.collapse", function (e) {
    drawAllDevices()
})

$('#navbarSupportedContent').on("hidden.bs.collapse", function (e) {
    drawAllDevices()
})

// just to make the mobile version nicer looking
window.onresize = function () {
    if (window.innerWidth < 442){
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('modalDropdownContainer'))
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('removeButton'))
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('undoButton'))
    }
    else {
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('modalDropdownContainer'))
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('removeButton'))
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('undoButton'))
    }
}

$(document).ready(function () {
    $('#dtUnregistered,#dtRegistered').DataTable({
        language: {
            lengthMenu: "Visa _MENU_ enheter per sida",
            zeroRecords: "Hittade ingen enhet",
            info: "Visar sida _PAGE_ av _PAGES_",
            infoEmpty: "Inga enheter tillgängliga",
            infoFiltered: "(filtrerad från _MAX_ enheter sammanlagt)"
        }
    })
    $('#datetimepicker1').datetimepicker()

    if (window.innerWidth < 442){
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('modalDropdownContainer'))
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('removeButton'))
        document.getElementById('mapButtonRow2').appendChild(document.getElementById('undoButton'))
    }
    else {
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('modalDropdownContainer'))
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('removeButton'))
        document.getElementById('mapButtonRow1').appendChild(document.getElementById('undoButton'))
    }
})