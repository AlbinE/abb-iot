const doc = require('aws-sdk');
const docClient = new doc.DynamoDB.DocumentClient({
    region: 'eu-north-1'
});

const config = {
    macRegex: /^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$/
};

/**
 * Returns true if a provided MAC address exists and is valid.
 */
function verifyMAC(event, callback) {
    // Make sure MAC address parameter exists
    if(!event.queryStringParameters.hasOwnProperty("MAC")) {
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "No 'MAC' query string provided. Examine your IoT device for its unique MAC address."
            })
        });
        return false;
    }
    
    // Check if MAC string has correct structure
    if(!config.macRegex.test(event.queryStringParameters.MAC)) {
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "Provided 'MAC' query was invalid. Example: A9:04:22:54:C2:F5. Examine your IoT device for its unique MAC address."
            })
        });
        return false;
    }
    
    return true;
}

// Entry function
exports.handler = (event, context, callback) => {
    if(!verifyMAC(event, callback))
        return;
    
    // Capitalize to avoid case sensitivity
    const macAddress = event.queryStringParameters.MAC.toUpperCase();
    
    console.log(`Searching for device with MAC address "${macAddress}"...`);
    
    docClient.get({
        TableName: "Devices",
        Key: {
            macAddress: macAddress
        }
    }, (err, data) => {
        if(err) {
            callback(err, {
                statusCode: 500, // Internal Server Error
                headers: {
                    'Content-Type': 'application/json'
                },
                body: "An internal database query has failed."
            });
            return;
        }
        
        // If device is known
        if(data.hasOwnProperty("Item")) {
            console.info("Device found.");
            var fileName = macAddress.replace(/:/g, '-');
            // Table headers, add or remove measurement types here
            var csv = 'Datum och Tid;Temperatur (°C);Luftfuktighet (%);Koldioxidhalt (ppm)\r\n';
            data.Item.data.forEach((dataPoint) => {
                var time = new Date(dataPoint.time * 1000)
                    .toISOString()
                    .replace('T', ' ')
                    .substring(0, 19);

                // --- TEMPERATURE ---
                var temperature = '';
                // Inserts measurement if it is present in the data.
                if(dataPoint.temperature) {
                    temperature += dataPoint.temperature;
                }
                // Excel interprets decimal points as commas instead of dots.
                temperature = temperature.replace('.', ',');
                // -------------------

                // --- HUMIDITY ---
                var humidity = '';
                if(dataPoint.humidity) {
                    humidity += dataPoint.humidity;
                }
                humidity = humidity.replace('.', ',');
                // ----------------

                // --- CARBON DIOXIDE ---
                var co2 = '';
                if(dataPoint.co2) {
                    co2 += dataPoint.co2;
                }
                co2 = co2.replace('.', ',');
                // ----------------------

                // Values are separated by semicolons.
                // Empty variables are interpreted as empty cells.
                csv += `${time};${temperature};${humidity};${co2}\r\n`;
            });
            callback(null, {
                statusCode: 200, // OK
                headers: {
                    'Content-Type': 'text/csv',
                    'Content-Disposition': `attachment; filename=${fileName}.csv`
                },
                body: csv
            });
        }
        else {
            console.error("No device found.");
            callback(null, {
                statusCode: 404, // Not Found
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    message: "Device not found."
                })
            });
        }
    });
};
