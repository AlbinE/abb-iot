const doc = require('aws-sdk');
const docClient = new doc.DynamoDB.DocumentClient({
    region: 'eu-north-1'
});

const config = {
    macRegex: /^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$/
};

/**
 * Returns true if a provided MAC address exists and is valid.
 */
function verifyMAC(event, callback) {
    // Make sure MAC address parameter exists
    if(!event.queryStringParameters.hasOwnProperty("MAC")) {
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "No 'MAC' query string provided. Examine your IoT device for its unique MAC address."
            })
        });
        return false;
    }
    
    // Check if MAC string has correct structure
    if(!config.macRegex.test(event.queryStringParameters.MAC)) {
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "Provided 'MAC' query was invalid. Example: A9:04:22:54:C2:F5. Examine your IoT device for its unique MAC address."
            })
        });
        return false;
    }
    
    return true;
}

// Entry function
exports.handler = (event, context, callback) => {
    if(!verifyMAC(event, callback))
        return;
    
    var interval;
    var name;
    var position;
    var map;
    var owner;
    // Check if body exists
    if(event.body) {
        const body = JSON.parse(event.body);

        // Acquire interval
        if(body.interval && typeof body.interval === "number") {
            interval = body.interval;

            if(interval < 10) {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The specified interval must be 10 seconds or greater."
                    })
                });
                return;
            }
        }

        // Acquire name
        if(body.name && typeof body.name === "string") {
            name = body.name;

            if(name === "") {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The specified name must not be empty."
                    })
                });
                return;
            }
        }

        // Acquire position
        if(body.position && typeof body.position === "object") {
            position = body.position;

            if(!body.position.x || typeof body.position.x !== "number"
            || !body.position.y || typeof body.position.y !== "number"
            || body.position.x < 0.0 || body.position.x > 1.0
            || body.position.y < 0.0 || body.position.y > 1.0
            || Object.keys(body.position).length !== 2) {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The position must contain two numbers 'x' and 'y' with values between 0.0 and 1.0."
                    })
                });
                return;
            }
        }

        // Acquire map
        if(body.map && typeof body.map === "string") {
            map = body.map;

            if(map === "") {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The specified map string must not be empty."
                    })
                });
                return;
            }
        }

        // Acquire owner
        if(body.owner && typeof body.owner === "string") {
            owner = body.owner;

            if(owner === "") {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The specified owner name must not be empty."
                    })
                });
                return;
            }
        }
    }
    else {
        console.error("No body in request.");
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "The request did not contain a body. No options were updated."
            })
        });
        return;
    }

    // Capitalize to avoid case sensitivity
    const macAddress = event.queryStringParameters.MAC.toUpperCase();
    
    console.log(`Updating options for device with MAC address "${macAddress}"...`);

    docClient.get({
        TableName: "Devices",
        Key: {
            macAddress: macAddress
        },
        ProjectionExpression: "isRegistered, macAddress, updateInterval, #n, #p, mapId, #o",
        ExpressionAttributeNames: {
            "#n": "name",
            "#p": "position",
            "#o": "owner"
        }
    }, (err, data) => {
        if(err) {
            callback(err, {
                statusCode: 500, // Internal Server Error
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    message: "An internal database query has failed."
                })
            });
            return;
        }
        
        // If device is known
        if(data.hasOwnProperty("Item")) {
            var item = data.Item;

            if(!item.isRegistered) {
                callback(null, {
                    statusCode: 400, // Bad Request
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        message: "The targeted device is not registered."
                    })
                });
                return;
            }

            console.info("Device found.");

            if(!interval) {
                interval = item.updateInterval;
            }

            if(!name) {
                name = item.name;
            }

            if(!position) {
                position = item.position;
            }

            if(!map) {
                map = item.mapId;
            }

            if(!owner) {
                owner = item.owner;
            }

            docClient.update({
                TableName: "Devices",
                Key: {
                    macAddress: macAddress
                },
                UpdateExpression: "SET #i = :i, #n = :n, #p = :p, #m = :m, #o = :o",
                ExpressionAttributeNames: {
                    "#i": "updateInterval",
                    "#n": "name",
                    "#p": "position",
                    "#m": "mapId",
                    "#o": "owner"
                },
                ExpressionAttributeValues: {
                    ":i": interval,
                    ":n": name,
                    ":p": position,
                    ":m": map,
                    ":o": owner
                }
            }, (err, data) => {
                if(err) {
                    callback(err, {
                        statusCode: 500, // Internal Server Error
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "message": "An internal database write has failed."
                        })
                    });
                    return;
                }

                callback(null, {
                    statusCode: 200, // OK
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "message": "Device options successfully changed."
                    })
                });
            });
        }
        else {
            console.error("No device found.");
            callback(null, {
                statusCode: 404, // Not Found
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    message: "Device not found."
                })
            });
        }
    });
};
