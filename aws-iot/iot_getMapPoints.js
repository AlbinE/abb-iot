const doc = require('aws-sdk');
const docClient = new doc.DynamoDB.DocumentClient({
    region: 'eu-north-1'
});

const config = {
};

/**
 * Returns true if a provided Map ID exists.
 */
function verifyMapID(event, callback) {
    // Make sure Map ID parameter exists
    if(!event.queryStringParameters.hasOwnProperty("MapID")) {
        callback(null, {
            statusCode: 400, // Bad Request
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                message: "No 'MapID' query string provided. Please provide a MapID to receive map points."
            })
        });
        return false;
    }
    
    return true;
}

// Entry function
exports.handler = (event, context, callback) => {
    if(!verifyMapID(event, callback))
        return;
    
    // Capitalize to avoid case sensitivity
    const mapId = event.queryStringParameters.MapID;
    
    console.log(`Getting points for map "${mapId}"...`);
    
    docClient.scan({
        TableName: "Devices",
        FilterExpression: 'mapId = :map',
        ExpressionAttributeValues: {
            ':map': mapId
        },
        ProjectionExpression: "macAddress, #name, #position, latestData",
        ExpressionAttributeNames: {
            '#name': 'name',
            '#position': 'position'
        }
    }, (err, data) => {
        if(err) {
            callback(err, {
                statusCode: 500, // Internal Server Error
                headers: {
                    'Content-Type': 'application/json'
                },
                body: "An internal database query has failed."
            });
            return;
        }
        callback(null, {
            statusCode: 200, // OK
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data["Items"])
        });
    });
};
